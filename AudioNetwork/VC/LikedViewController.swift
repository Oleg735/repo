//
//  LikedViewController.swift
//  AudioNetwork
//
//  Created by user on 28.07.2021.
//

import UIKit
import CoreData

class LikedViewController: UIViewController {

    var tasksArray: [TasksCore] = []

    var refreshContr: UIRefreshControl?
  
    
    @IBOutlet weak var tableV: UITableView!
    
    private var filteredArray = [TasksCore]()
    private let searchController = UISearchController(searchResultsController: nil)
    private var searchBarIsEmpty: Bool {
        guard let text = searchController.searchBar.text else { return false }
        return text.isEmpty
    }
    private var isFiltering: Bool {
        let searchBarScopeFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty || searchBarScopeFiltering)
    }
 
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let appDeleg = UIApplication.shared.delegate as! AppDelegate
        let context = appDeleg.persistentContainer.viewContext

        let fetchRequest: NSFetchRequest<TasksCore> = TasksCore.fetchRequest()
        do {
            tasksArray = try context.fetch(fetchRequest)
        } catch let error as NSError{
            print(error.localizedDescription)
        }
        tableV.reloadData()

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //tableView.tableFooterView = UIView(frame: .zero)
        
        tableV.delegate = self
        tableV.dataSource = self
        
        addRefreshControl()
    
        createSearchAndScoreBar()
    }
    //MARK: - Refresh
    func addRefreshControl() {
        refreshContr = UIRefreshControl()
        refreshContr?.tintColor = UIColor.red
        refreshContr?.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
        tableV.addSubview(refreshContr!)
    }
    
    @objc func refreshAction() {
        tableV.reloadData()
        refreshContr?.endRefreshing()
    }
    
    @IBAction func trashAction(_ sender: Any) {
        let appDeleg = UIApplication.shared.delegate as! AppDelegate
        let context = appDeleg.persistentContainer.viewContext

        let fetchRequest: NSFetchRequest<TasksCore> = TasksCore.fetchRequest()
        if let tasks = try? context.fetch(fetchRequest) {
            for task in tasks {
                context.delete(task)
            }
        }
        do {
            try context.save()
        } catch let error {
            print(error.localizedDescription)
        }
        tableV.reloadData()
    }
    
    func createSearchAndScoreBar() {
        // Setup searchController
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "search"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        //Setup the Score Bar
        searchController.searchBar.scopeButtonTitles = ["All", "song", "feature-movie", "podcast"]
        let font = UIFont.systemFont(ofSize: 9)
        searchController.searchBar.setScopeBarButtonTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        searchController.searchBar.delegate = self
        
    }
    //MARK: - go to site
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showWK" {
            if let indexP = tableV.indexPathForSelectedRow {

                let itemForTransfer: TasksCore

                if isFiltering {
                    itemForTransfer = filteredArray[indexP.row]
                } else {
                    itemForTransfer = tasksArray[indexP.row]
                }

                let vc = segue.destination as? ShowViewController
                vc?.transferFromCore = itemForTransfer
            }
        }
    }


    

    // MARK: - Table view data source

//    override func numberOfSections(in tableView: UITableView) -> Int {
//
//        return 0
//    }
}
    extension LikedViewController: UITableViewDelegate, UITableViewDataSource {
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

            let transf = tasksArray[indexPath.row]
            self.performSegue(withIdentifier: "showWK", sender: transf)

        }
        
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if isFiltering {
            return filteredArray.count
        }
        return tasksArray.count
    }

     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if  let cell = tableView.dequeueReusableCell(withIdentifier: "SavedCell", for: indexPath) as? LikedTableViewCell {
            let item: TasksCore
        
          
            if isFiltering {
                item = filteredArray[indexPath.row]
            }else {
                item = tasksArray[indexPath.row]
            }
            
            cell.configure(item)
        
            return cell
        }
        return LikedTableViewCell()
       
    }

     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
      
}
//MARK: - UISearchResultsUpdating
extension LikedViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        
        let searchBar = searchController.searchBar
        let scope1 = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearch(searchController.searchBar.text!, scope: scope1)
    }
    
    private func filterContentForSearch(_ searchText: String, scope: String = "All") {
        
        filteredArray = tasksArray.filter({ (tasksArray: TasksCore) -> Bool in
            
            let doesCategoryMatch = (scope == "All") || (tasksArray.kindCore! == scope)
            
            if searchBarIsEmpty {
                return doesCategoryMatch
            }
            
            return doesCategoryMatch && (tasksArray.trackNameCore?.lowercased().contains(searchText.lowercased()) ?? false ||  tasksArray.artistNameCore?.lowercased().contains(searchText.lowercased()) ?? false)
        })
        
        tableV.reloadData()
    }
    
}
//MARK: - UISearchBar Delegate
extension LikedViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearch(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
}



