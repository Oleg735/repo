//
//  ViewController.swift
//  AudioNetwork
//
//  Created by user on 27.07.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var itemArray = [SearchResponse]() {
        didSet {
            tableView.reloadData()
        }
    }
       
    @IBOutlet weak var label: UILabel!
    
    var refreshContr: UIRefreshControl?
    let searchController = UISearchController(searchResultsController: nil)
    private var timer: Timer?
    
    private var searchBarIsEmpty: Bool {
        guard let text = searchController.searchBar.text else { return false }
        return text.isEmpty
    }
    
    private var isFiltering: Bool {
        return searchController.isActive && !searchBarIsEmpty
    }
    
    let networkManag = NetworkManager()
    var searchResponse: SearchResponse? = nil
    
    var limitCount = 10
    var total = 200
    var search: String = ""
    var baseURLSearch: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let baseURL = "https://newsapi.org/v2/everything?q=bitcoin&apiKey=d73af9dbb89b4bc28461941746255946&pageSize=10&page=1"
       
        
        responseFunc(search: "jack+johnson", limit: limitCount)
        
        setupTableView()
        setupSearchBar()
//        networkManag.getAllPosts { [weak self] (posts) in
//            DispatchQueue.main.async {
//                self?.itemArray = posts
//                print("posts  \(posts)")
//            }
//        }
        addRefreshControl()
        
        configureLabel()
       
    }
    func responseFunc( search: String, limit: Int) {
        let baseURL = "https://itunes.apple.com/search?term=\(search)&limit=\(limit)"
        self.networkManag.getAllPosts(urlString: baseURL) { [weak self] (result) in
            switch result {
            case .success(let searchResponse):
                self?.searchResponse = searchResponse
                //self?.itemArray = [searchResponse]
                self?.tableView.reloadData()
            case .failure(let error):
                print("error:", error)
            }
        }
    }
    

    func addRefreshControl() {
        refreshContr = UIRefreshControl()
        refreshContr?.tintColor = UIColor.red
        refreshContr?.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
        tableView.addSubview(refreshContr!)
    }
    
    @objc func refreshAction() {
        if isFiltering {
            limitCount = 10
            responseFunc(search: search, limit: limitCount)
            tableView.reloadData()
            refreshContr?.endRefreshing()
            label.alpha = 0
        } else {
            limitCount = 10
            responseFunc(search: "jack+johnson", limit: limitCount)
            tableView.reloadData()
            refreshContr?.endRefreshing()
            label.alpha = 0
        }
    }
    
    private func configureLabel() {
        label.alpha = 0
        label.text = "No item match your query"
    }
    
    private func setupSearchBar() {
        navigationItem.searchController = searchController
        searchController.searchBar.delegate = self
        navigationController?.navigationBar.prefersLargeTitles = true
        searchController.obscuresBackgroundDuringPresentation = false
    }
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
//        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    @IBAction func savedList(_ sender: Any) {
        self.performSegue(withIdentifier: "Saved", sender: nil)
    }
//
//    @IBAction func editAction(_ sender: Any) {
//        tableView.isEditing = !tableView.isEditing
//    }
//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showVC" {
            if let vc = segue.destination as? ShowViewController {
                let menu = sender as? Track
                
                print(menu ?? "nil")
                vc.transfer = menu
            }
        }
    }
}
    // MARK: - Table view data source
//    override func numberOfSections(in tableView: UITableView) -> Int {
//
//    }

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if isFiltering {
//            searchFooter.setIsFilteringToShow(filteredItemCount: searchResponse?.results.count ?? 0 , of: total)
//        } else {
//            //searchFooter.setNotFiltering()
//        }
        //print(searchResponse?.results.count ?? 0)
        return searchResponse?.results.count ?? 0
        
        
     }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if limitCount < total && indexPath.row == (searchResponse?.results.count)! - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "loading", for: indexPath)

            return cell
        } else  {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell

            let track = searchResponse?.results[indexPath.row]
            cell.configure(track!)
   
            return cell
            }
        return TableViewCell()
            
        }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if isFiltering {
            if self.limitCount < self.total && indexPath.row == (self.searchResponse?.results.count)! - 1 {
                self.limitCount = self.limitCount + 10
                self.responseFunc(search: search, limit: limitCount)
            }
        } else {
            if self.limitCount < self.total && indexPath.row == (self.searchResponse?.results.count)! - 1 {
                self.limitCount = self.limitCount + 10
                responseFunc(search: "jack+johnson", limit: limitCount)
            }
        }
      
    }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 139
    }
        
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let menu = itemArray[indexPath.row]
        let menu = searchResponse?.results[indexPath.row]
        self.performSegue(withIdentifier: "showVC", sender: menu)
        
    }

}
extension ViewController: UISearchBarDelegate {
       
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //let baseURL = "https://newsapi.org/v2/everything?q=bitcoin&apiKey=d73af9dbb89b4bc28461941746255946&pageSize=10&page=1"
        limitCount = 10
        let baseURL1 = "https://itunes.apple.com/search?term=\(searchText)&limit=200"
//        baseURLSearch = "https://itunes.apple.com/search?term=\(searchText)&limit=\(limitCount)"
        search = searchText
        
        self.networkManag.getAllPosts(urlString: baseURL1) { [weak self] (result) in
            switch result {
            case .success(let searchResponse):
                self?.total = searchResponse.resultCount
                print((self?.total)!)
                //self?.searchResponse = searchResponse
                self?.tableView.reloadData()
            case .failure(let error):
                print("error:", error)
            }
        }
        
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (_) in
            
            self.responseFunc(search: self.search, limit: self.limitCount)
            if searchText.count == 0 {
                self.label.alpha = 1
            } else {
                self.label.alpha = 0
            }
        })
        
        
    }
}


