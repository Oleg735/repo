//
//  Model.swift
//  AudioNetwork
//
//  Created by user on 27.07.2021.
//

import Foundation
//
//struct ArticlesResponseModel: Decodable {
//    let status: String
//    let articles: [ArticleModel]
//}
//
//struct ArticleModel: Decodable {
//    let source: SourceResponseModel
//    let title: String
//    let description: String?
//}
//
//struct SourceResponseModel: Decodable {
//    let id: String
//    let name: String
//}



struct SearchResponse: Decodable {
    var resultCount: Int
    var results: [Track]
    	
}

struct Track: Decodable {
    var trackName: String?
    //var collectionName: String?
    var artistName: String
    var artworkUrl100: String?
    var trackViewUrl: String?
    var previewUrl: String?
    var kind: String?
    var releaseDate: String?
}
