//
//  TableViewCell.swift
//  AudioNetwork
//
//  Created by user on 27.07.2021.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var trackLabel: UILabel!
    @IBOutlet weak var kindLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setImage(urlName: String){
        guard let imageUrl = URL(string: urlName) else { return }
        let data = try! Data(contentsOf: imageUrl)
        let image = UIImage(data: data)
        
        imageV.image = image
    }
    
    func setStr(labelStr: String) -> String{
        let string = labelStr
        if string.count >= 7 {
            let firstCharIndex = string.index(string.startIndex, offsetBy: 7)
            let firstChar = string.substring(to: firstCharIndex)
            return firstChar
        }
        return string
    }
    
    func configure( _ model: Track) {
        setImage(urlName: model.artworkUrl100!)
        titleLabel.text = model.artistName
        trackLabel.text = model.trackName
        kindLabel.text = model.kind
        releaseDateLabel.text = setStr(labelStr: model.releaseDate!)
        
    }
}
